# `terraform-tutorial`

## Prerequisites

```sh
$ brew tap hashicorp/tap
$ brew install hashicorp/tap/terraform
$ brew install terragrunt

$ open https://console.cloud.yandex.com/ # register
$ curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
$ yc init
```
