locals {
  yc_token = get_env("YC_TOKEN")
  yc_cloud_id  = get_env("YC_CLOUD_ID")
  yc_folder_dev_id = get_env("YC_FOLDER_ID")
  yc_zone      = "ru-central1-a"
}

generate "provider" {
  path      = "provider_gen.tf"
  if_exists = "overwrite"
  contents  = <<EOF
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "${local.yc_token}"
  cloud_id  = "${local.yc_cloud_id}"
  folder_id = "${local.yc_folder_dev_id}"
  zone      = "${local.yc_zone}"
}
EOF
}
