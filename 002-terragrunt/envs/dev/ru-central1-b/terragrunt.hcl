terraform {
  source = "../../../modules/yandex"
}

include "root" {
  path = find_in_parent_folders()
  expose = true
}

inputs = {
  // servers      = include.root.locals.servers
  main-vm-name = "main-b"
  zone = "ru-central1-b"
  subnet_id = "e2lram65se6gb5kjb52p"
}
