terraform {
  source = "../../../modules/yandex"
}

include "root" {
  path = find_in_parent_folders()
  expose = true
}

inputs = {
  // servers      = include.root.locals.servers
  main-vm-name = "main-c"
  zone = "ru-central1-c"
  subnet_id = "b0cg28na0khomuhu1esd"
}
