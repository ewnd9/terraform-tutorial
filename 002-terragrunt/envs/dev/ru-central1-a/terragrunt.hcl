terraform {
  source = "../../../modules/yandex"
}

include "root" {
  path = find_in_parent_folders()
  expose = true
}

inputs = {
  // servers      = include.root.locals.servers
  main-vm-name = "main-a"
  zone = "ru-central1-a"
  subnet_id = "e9bf52h955i7q55f3cdf"
}
