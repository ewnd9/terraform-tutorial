package main

import "time"

// https://transform.tools/json-to-go
type TerraformPlan struct {
	// FormatVersion    string `json:"format_version"`
	// TerraformVersion string `json:"terraform_version"`
	// Variables        struct {
	// 	MainVMName struct {
	// 		Value string `json:"value"`
	// 	} `json:"main-vm-name"`
	// 	SubnetID struct {
	// 		Value string `json:"value"`
	// 	} `json:"subnet_id"`
	// 	Zone struct {
	// 		Value string `json:"value"`
	// 	} `json:"zone"`
	// } `json:"variables"`
	// PlannedValues struct {
	// 	RootModule struct {
	// 		Resources []struct {
	// 			Address       string `json:"address"`
	// 			Mode          string `json:"mode"`
	// 			Type          string `json:"type"`
	// 			Name          string `json:"name"`
	// 			ProviderName  string `json:"provider_name"`
	// 			SchemaVersion int    `json:"schema_version"`
	// 			Values        struct {
	// 				AllowRecreate          interface{} `json:"allow_recreate"`
	// 				AllowStoppingForUpdate interface{} `json:"allow_stopping_for_update"`
	// 				BootDisk               []struct {
	// 					AutoDelete       bool `json:"auto_delete"`
	// 					InitializeParams []struct {
	// 						ImageID string `json:"image_id"`
	// 						Type    string `json:"type"`
	// 					} `json:"initialize_params"`
	// 				} `json:"boot_disk"`
	// 				Description             interface{}   `json:"description"`
	// 				Filesystem              []interface{} `json:"filesystem"`
	// 				Labels                  interface{}   `json:"labels"`
	// 				LocalDisk               []interface{} `json:"local_disk"`
	// 				Metadata                interface{}   `json:"metadata"`
	// 				Name                    string        `json:"name"`
	// 				NetworkAccelerationType string        `json:"network_acceleration_type"`
	// 				NetworkInterface        []struct {
	// 					DNSRecord     []interface{} `json:"dns_record"`
	// 					Ipv4          bool          `json:"ipv4"`
	// 					Ipv6DNSRecord []interface{} `json:"ipv6_dns_record"`
	// 					Nat           bool          `json:"nat"`
	// 					NatDNSRecord  []interface{} `json:"nat_dns_record"`
	// 					SubnetID      string        `json:"subnet_id"`
	// 				} `json:"network_interface"`
	// 				PlatformID string `json:"platform_id"`
	// 				Resources  []struct {
	// 					CoreFraction int         `json:"core_fraction"`
	// 					Cores        int         `json:"cores"`
	// 					Gpus         interface{} `json:"gpus"`
	// 					Memory       int         `json:"memory"`
	// 				} `json:"resources"`
	// 				SecondaryDisk []interface{} `json:"secondary_disk"`
	// 				Timeouts      interface{}   `json:"timeouts"`
	// 				Zone          string        `json:"zone"`
	// 			} `json:"values"`
	// 			SensitiveValues struct {
	// 				BootDisk []struct {
	// 					InitializeParams []struct {
	// 					} `json:"initialize_params"`
	// 				} `json:"boot_disk"`
	// 				Filesystem       []interface{} `json:"filesystem"`
	// 				LocalDisk        []interface{} `json:"local_disk"`
	// 				MetadataOptions  []interface{} `json:"metadata_options"`
	// 				NetworkInterface []struct {
	// 					DNSRecord        []interface{} `json:"dns_record"`
	// 					Ipv6DNSRecord    []interface{} `json:"ipv6_dns_record"`
	// 					NatDNSRecord     []interface{} `json:"nat_dns_record"`
	// 					SecurityGroupIds []interface{} `json:"security_group_ids"`
	// 				} `json:"network_interface"`
	// 				PlacementPolicy []interface{} `json:"placement_policy"`
	// 				Resources       []struct {
	// 				} `json:"resources"`
	// 				SchedulingPolicy []interface{} `json:"scheduling_policy"`
	// 				SecondaryDisk    []interface{} `json:"secondary_disk"`
	// 			} `json:"sensitive_values"`
	// 		} `json:"resources"`
	// 	} `json:"root_module"`
	// } `json:"planned_values"`
	ResourceChanges []struct {
		Address      string `json:"address"`
		Mode         string `json:"mode"`
		Type         string `json:"type"`
		Name         string `json:"name"`
		ProviderName string `json:"provider_name"`
		Change       struct {
			Actions []string `json:"actions"`
			// Before  interface{} `json:"before"`
			// After   struct {
			// 	AllowRecreate          interface{} `json:"allow_recreate"`
			// 	AllowStoppingForUpdate interface{} `json:"allow_stopping_for_update"`
			// 	BootDisk               []struct {
			// 		AutoDelete       bool `json:"auto_delete"`
			// 		InitializeParams []struct {
			// 			ImageID string `json:"image_id"`
			// 			Type    string `json:"type"`
			// 		} `json:"initialize_params"`
			// 	} `json:"boot_disk"`
			// 	Description             interface{}   `json:"description"`
			// 	Filesystem              []interface{} `json:"filesystem"`
			// 	Labels                  interface{}   `json:"labels"`
			// 	LocalDisk               []interface{} `json:"local_disk"`
			// 	Metadata                interface{}   `json:"metadata"`
			// 	Name                    string        `json:"name"`
			// 	NetworkAccelerationType string        `json:"network_acceleration_type"`
			// 	NetworkInterface        []struct {
			// 		DNSRecord     []interface{} `json:"dns_record"`
			// 		Ipv4          bool          `json:"ipv4"`
			// 		Ipv6DNSRecord []interface{} `json:"ipv6_dns_record"`
			// 		Nat           bool          `json:"nat"`
			// 		NatDNSRecord  []interface{} `json:"nat_dns_record"`
			// 		SubnetID      string        `json:"subnet_id"`
			// 	} `json:"network_interface"`
			// 	PlatformID string `json:"platform_id"`
			// 	Resources  []struct {
			// 		CoreFraction int         `json:"core_fraction"`
			// 		Cores        int         `json:"cores"`
			// 		Gpus         interface{} `json:"gpus"`
			// 		Memory       int         `json:"memory"`
			// 	} `json:"resources"`
			// 	SecondaryDisk []interface{} `json:"secondary_disk"`
			// 	Timeouts      interface{}   `json:"timeouts"`
			// 	Zone          string        `json:"zone"`
			// } `json:"after"`
			// AfterUnknown struct {
			// 	BootDisk []struct {
			// 		DeviceName       bool `json:"device_name"`
			// 		DiskID           bool `json:"disk_id"`
			// 		InitializeParams []struct {
			// 			BlockSize   bool `json:"block_size"`
			// 			Description bool `json:"description"`
			// 			Name        bool `json:"name"`
			// 			Size        bool `json:"size"`
			// 			SnapshotID  bool `json:"snapshot_id"`
			// 		} `json:"initialize_params"`
			// 		Mode bool `json:"mode"`
			// 	} `json:"boot_disk"`
			// 	CreatedAt        bool          `json:"created_at"`
			// 	Filesystem       []interface{} `json:"filesystem"`
			// 	FolderID         bool          `json:"folder_id"`
			// 	Fqdn             bool          `json:"fqdn"`
			// 	GpuClusterID     bool          `json:"gpu_cluster_id"`
			// 	Hostname         bool          `json:"hostname"`
			// 	ID               bool          `json:"id"`
			// 	LocalDisk        []interface{} `json:"local_disk"`
			// 	MetadataOptions  bool          `json:"metadata_options"`
			// 	NetworkInterface []struct {
			// 		DNSRecord        []interface{} `json:"dns_record"`
			// 		Index            bool          `json:"index"`
			// 		IPAddress        bool          `json:"ip_address"`
			// 		Ipv6             bool          `json:"ipv6"`
			// 		Ipv6Address      bool          `json:"ipv6_address"`
			// 		Ipv6DNSRecord    []interface{} `json:"ipv6_dns_record"`
			// 		MacAddress       bool          `json:"mac_address"`
			// 		NatDNSRecord     []interface{} `json:"nat_dns_record"`
			// 		NatIPAddress     bool          `json:"nat_ip_address"`
			// 		NatIPVersion     bool          `json:"nat_ip_version"`
			// 		SecurityGroupIds bool          `json:"security_group_ids"`
			// 	} `json:"network_interface"`
			// 	PlacementPolicy bool `json:"placement_policy"`
			// 	Resources       []struct {
			// 	} `json:"resources"`
			// 	SchedulingPolicy bool          `json:"scheduling_policy"`
			// 	SecondaryDisk    []interface{} `json:"secondary_disk"`
			// 	ServiceAccountID bool          `json:"service_account_id"`
			// 	Status           bool          `json:"status"`
			// } `json:"after_unknown"`
			// BeforeSensitive bool `json:"before_sensitive"`
			// AfterSensitive  struct {
			// 	BootDisk []struct {
			// 		InitializeParams []struct {
			// 		} `json:"initialize_params"`
			// 	} `json:"boot_disk"`
			// 	Filesystem       []interface{} `json:"filesystem"`
			// 	LocalDisk        []interface{} `json:"local_disk"`
			// 	MetadataOptions  []interface{} `json:"metadata_options"`
			// 	NetworkInterface []struct {
			// 		DNSRecord        []interface{} `json:"dns_record"`
			// 		Ipv6DNSRecord    []interface{} `json:"ipv6_dns_record"`
			// 		NatDNSRecord     []interface{} `json:"nat_dns_record"`
			// 		SecurityGroupIds []interface{} `json:"security_group_ids"`
			// 	} `json:"network_interface"`
			// 	PlacementPolicy []interface{} `json:"placement_policy"`
			// 	Resources       []struct {
			// 	} `json:"resources"`
			// 	SchedulingPolicy []interface{} `json:"scheduling_policy"`
			// 	SecondaryDisk    []interface{} `json:"secondary_disk"`
			// } `json:"after_sensitive"`
		} `json:"change"`
	} `json:"resource_changes"`
	// Configuration struct {
	// 	ProviderConfig struct {
	// 		Yandex struct {
	// 			Name        string `json:"name"`
	// 			FullName    string `json:"full_name"`
	// 			Expressions struct {
	// 				CloudID struct {
	// 					ConstantValue string `json:"constant_value"`
	// 				} `json:"cloud_id"`
	// 				FolderID struct {
	// 					ConstantValue string `json:"constant_value"`
	// 				} `json:"folder_id"`
	// 				Token struct {
	// 					ConstantValue string `json:"constant_value"`
	// 				} `json:"token"`
	// 				Zone struct {
	// 					ConstantValue string `json:"constant_value"`
	// 				} `json:"zone"`
	// 			} `json:"expressions"`
	// 		} `json:"yandex"`
	// 	} `json:"provider_config"`
	// 	RootModule struct {
	// 		Resources []struct {
	// 			Address           string `json:"address"`
	// 			Mode              string `json:"mode"`
	// 			Type              string `json:"type"`
	// 			Name              string `json:"name"`
	// 			ProviderConfigKey string `json:"provider_config_key"`
	// 			Expressions       struct {
	// 				BootDisk []struct {
	// 					InitializeParams []struct {
	// 						ImageID struct {
	// 							ConstantValue string `json:"constant_value"`
	// 						} `json:"image_id"`
	// 					} `json:"initialize_params"`
	// 				} `json:"boot_disk"`
	// 				Name struct {
	// 					References []string `json:"references"`
	// 				} `json:"name"`
	// 				NetworkInterface []struct {
	// 					Nat struct {
	// 						ConstantValue bool `json:"constant_value"`
	// 					} `json:"nat"`
	// 					SubnetID struct {
	// 						References []string `json:"references"`
	// 					} `json:"subnet_id"`
	// 				} `json:"network_interface"`
	// 				Resources []struct {
	// 					Cores struct {
	// 						ConstantValue int `json:"constant_value"`
	// 					} `json:"cores"`
	// 					Memory struct {
	// 						ConstantValue int `json:"constant_value"`
	// 					} `json:"memory"`
	// 				} `json:"resources"`
	// 				Zone struct {
	// 					References []string `json:"references"`
	// 				} `json:"zone"`
	// 			} `json:"expressions"`
	// 			SchemaVersion int `json:"schema_version"`
	// 		} `json:"resources"`
	// 		Variables struct {
	// 			MainVMName struct {
	// 				Description string `json:"description"`
	// 			} `json:"main-vm-name"`
	// 			SubnetID struct {
	// 				Description string `json:"description"`
	// 			} `json:"subnet_id"`
	// 			Zone struct {
	// 				Description string `json:"description"`
	// 			} `json:"zone"`
	// 		} `json:"variables"`
	// 	} `json:"root_module"`
	// } `json:"configuration"`
	Timestamp time.Time `json:"timestamp"`
}
