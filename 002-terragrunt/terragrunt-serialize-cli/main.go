package main

import (
	"encoding/json"
	"log"
	"os"
	"strings"
)

func main() {
	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	for _, arg := range os.Args[1:] {
		if arg == "-clean" {
			err := cleanTerragruntCache()
			if err != nil {
				return err
			}
		}

		if arg == "-plan" {
			err := runShell("terragrunt run-all plan -out tfplan && rm ./tfplan")
			if err != nil {
				return err
			}
		}

		if arg == "-apply" {
			err := applyChangedTerragruntPlans()
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func cleanTerragruntCache() error {
	// https://terragrunt.gruntwork.io/docs/features/caching/
	err := runShell("find . -type d -name \".terragrunt-cache\" -prune -exec rm -rf {} \\;")
	if err != nil {
		return err
	}

	return nil
}

func applyChangedTerragruntPlans() error {
	files, err := glob("**/*/tfplan")
	if err != nil {
		return err
	}

	for _, file := range files {
		bytes, err := runShellStdoutJson("terragrunt show -json " + file)
		if err != nil {
			return err
		}

		var plan TerraformPlan
		err = json.Unmarshal(bytes, &plan)
		if err != nil {
			return err
		}

		hasChanges := false
		for _, change := range plan.ResourceChanges {
			for _, action := range change.Change.Actions {
				if action != "no-op" {
					hasChanges = true
				}
			}
		}

		if hasChanges {
			fileParts := strings.Split(file, "/")
			fileDir := strings.Join(fileParts[0:len(fileParts)-1], "/")

			// 4 last parts "*/.terragrunt-cache/hash1/hash2/tfplan"
			outputFile := strings.Join(fileParts[0:len(fileParts)-4], "-")

			err := runShell("mkdir -p terragrunt-plans && cp -R " + fileDir + " terragrunt-plans/" + outputFile)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
