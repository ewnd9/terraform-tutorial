package main

import (
	"io/fs"
	"os"
	"os/exec"
	"regexp"
)

func runShell(command string) error {
	cmd := exec.Command("/bin/bash", "-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

func runShellStdoutJson(command string) ([]byte, error) {
	cmd := exec.Command("/bin/bash", "-c", command)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	return output, nil
}

// https://github.com/golang/go/issues/11862#issuecomment-1517792571
var replaces = regexp.MustCompile(`(\.)|(\*\*\/)|(\*)|([^\/\*]+)|(\/)`)

// https://github.com/golang/go/issues/11862#issuecomment-1517792571
func toRegexp(pattern string) string {
	pat := replaces.ReplaceAllStringFunc(pattern, func(s string) string {
		switch s {
		case "/":
			return "\\/"
		case ".":
			return "\\."
		case "**/":
			return ".*"
		case "*":
			return "[^/]*"
		default:
			return s
		}
	})
	return "^" + pat + "$"
}

// https://github.com/golang/go/issues/11862#issuecomment-1517792571
// Glob returns a list of files matching the pattern.
// The pattern can include **/ to match any number of directories.
func glob(pattern string) ([]string, error) {
	files := []string{}
	regexpPat := regexp.MustCompile(toRegexp(pattern))

	cwd, err := os.Getwd()
	if err != nil {
		return files, err
	}

	err = fs.WalkDir(os.DirFS(cwd), ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() || err != nil {
			return nil
		}
		if regexpPat.MatchString(path) {
			files = append(files, path)
		}
		return nil
	})

	return files, err
}
