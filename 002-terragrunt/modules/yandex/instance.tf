resource "yandex_compute_instance" "vm-1" {
  name = var.main-vm-name
  zone = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      # debian-9-v20211213
      image_id = "fd800n45ob5uggkrooi8"
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }
}
