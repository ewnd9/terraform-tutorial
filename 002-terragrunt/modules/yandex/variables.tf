variable "main-vm-name" {
  type        = string
  description = "main-vm-name"
}

variable "subnet_id" {
  type        = string
  description = "subnet_id"
}

variable "zone" {
  type        = string
  description = "zone"
}
