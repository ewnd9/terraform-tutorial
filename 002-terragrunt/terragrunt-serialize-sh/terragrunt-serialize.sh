#!/bin/sh

set -e

# Usage:
# (source terragrunt-serialize.sh && tg_plan_all)

tg_plan_all() {
  terragrunt run-all plan -out tfplan
  # terragrunt runs terraform plan in each directory
  # and copies last of them to the root, we don't need it
  # https://github.com/gruntwork-io/terragrunt/issues/720#issuecomment-497888756
  rm ./tfplan
}

tg_apply_all() {
  rm -rf terragrunt-plans
  mkdir terragrunt-plans

  find . -name "tfplan" -type f | while read tfplan_path; do
    changes_count=$(terraform show -json $tfplan_path | jq '.resource_changes[].change.actions | map(select(. != "no-op")) | length')

    if [[ $changes_count -gt 0 ]]; then
      dir_path=$(dirname $tfplan_path)
      echo "has changes $dir_path"
      dir_without_hashes=$(echo "$dir_path" | sed 's|/\.terragrunt-cache/.*$||')
      dir_without_leading_slash=$(echo "$dir_without_hashes" | sed 's|^./||')
      dir_without_slash=$(echo "$dir_without_leading_slash" | tr '/' '-')

      cp -R $dir_path terragrunt-plans/$dir_without_slash
    fi
  done
}

