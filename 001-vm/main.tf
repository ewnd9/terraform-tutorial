terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}

resource "yandex_compute_instance" "vm-1" {
  name = "terraform-vm-1"
  zone = "ru-central1-c"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      # debian-9-v20211213
      image_id = "fd800n45ob5uggkrooi8"
    }
  }

  network_interface {
    subnet_id = "b0cg28na0khomuhu1esd"
    nat       = true
  }
}
