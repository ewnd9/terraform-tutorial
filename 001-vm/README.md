# `001-basics`

## Links

- [Использование модулей Yandex Cloud в Terraform](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-modules)

## Prerequisites

```sh
$ yc iam service-account create --name demo-account
$ yc iam service-account list
# all roles https://cloud.yandex.com/en-ru/docs/resource-manager/security/
$ yc resource-manager folder add-access-binding b1gnsd8doqeniekq2ocu \
  --role resource-manager.admin \
  --subject serviceAccount:aje8to4vk35cmb9raghq
$ yc resource-manager folder add-access-binding b1gnsd8doqeniekq2ocu \
  --role vpc.admin \
  --subject serviceAccount:aje8to4vk35cmb9raghq

$ yc iam key create \
  --service-account-id aje8to4vk35cmb9raghq \
  --folder-name default \
  --output key.json

$ yc config profile create sa-terraform

$ yc config set service-account-key key.json
$ yc config set cloud-id b1g6490okg638i3b6kgp
$ yc config set folder-id b1gnsd8doqeniekq2ocu

$ export YC_TOKEN=$(yc iam create-token)
$ export YC_CLOUD_ID=$(yc config get cloud-id)
$ export YC_FOLDER_ID=$(yc config get folder-id)

$ cp .terraformrc ~/.terraformrc
```
